import { exec, spawn } from "child_process";
import { watch } from "chokidar"
import { Socket } from "net";
import { basename, join } from "path/posix";

var watcher = watch(join(__dirname, "scripts"), { ignored: /^\./, persistent: true });

let updates_queued: Set<string> = new Set()
let running_updates: Set<string> = new Set()

function update(path: string) {
    running_updates.add(path)
    const child = spawn("node", [join(__dirname, "dist/source/index.js"), path])
    child.stderr.pipe(process.stderr)
    child.stdout.pipe(process.stdout)

    console.log("=== CODE START ===");

    let buffer = ""
    child.stdout.on("data", (d) => {
        const s = d.toString()
        // process.stdout.write(d)
        buffer += s
    })
    child.on("exit", () => {
        console.log("==== CODE END ====");
        console.log("compilation completed");
        var client = new Socket();
        client.connect(6969, process.env.UPLOAD_SERVER ?? "doesnotexist.org", () => {
            console.log("connected for upload");
            client.write(buffer)
            client.end()
        });
        client.on("close", () => {
            console.log("upload finished");
            console.log();
            setTimeout(() => {

            })
            setTimeout(() => {
                running_updates.delete(path)
                if (updates_queued.has(path)) {
                    updates_queued.delete(path)
                    update(path)
                }
            }, 1200)
        })
    })
}

watcher
    .on('add', (path: string) => {
        console.log(`added ${basename(path)}`);
    })
    .on('change', (path: string) => {
        console.log(`changed ${basename(path)}`);
        if (!path.endsWith("cxx")) return
        if (updates_queued.has(path)) {
            console.log("-> updated already queued");
        } else if (running_updates.has(path)) {
            updates_queued.add(path)
            console.log("-> update running, queued compilation");
        } else {
            console.log("-> compiling");
            update(path)
        }

    })
    .on('unlink', (path: string) => {
        console.log(`removed ${basename(path)}`);
    })
    .on('error', (error: string) => { console.error('error ', error); })


