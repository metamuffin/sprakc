import { NodeKind, AST } from "./ast";
import { indent } from "./helper";

export function transpile_type(type: string): string {
    if (type == "Number") return "number"
    if (type == "String") return "string"
    if (type == "Bool") return "boolean"
    if (type == "void") return "void"
    if (type.startsWith("Array<") && type.endsWith(">")) return "array"
    return "var"
}

export function transpile(node: AST): string {
    const transpile_nth_inner = (i: number) => {
        if (!node.inner) throw new Error("aaaaaaahjhgfasjd");
        const k = node.inner[i]
        if (!k) throw new Error("asdfhasdkjf");
        return transpile(k)
    }
    const transpile_first_inner = () => transpile_nth_inner(0)
    const transpile_all_inner = () => {
        if (!node.inner) return ""
        return node.inner.map(n => transpile(n)).filter(n => n.length).join("\n")
    }

    if (node.implicit) return ""

    const V: { [key in NodeKind]?: () => string } = {
        TranslationUnitDecl: transpile_all_inner,
        TypedefDecl: () => "",
        CXXRecordDecl: () => "",
        FunctionDecl: () => {
            if (!node.type || !node.type.qualType) throw new Error("asidfjhkd");
            if (!node.name) throw new Error("sdarsudfkjbvnmc");
            const ret_type = node.type.qualType.split(" ")[0]
            const name = node.name
            const body = node.inner?.find(i => i.kind == "CompoundStmt")
            if (!body) return ""
            if (!node.inner) throw new Error("sdahfjakshfd");
            const args: [string, string][] = node.inner.filter(i => i.kind != "CompoundStmt").map(i => [i.name ?? "ohnonononono", i.type?.qualType ?? "var"])
            if (node.name == "main") return transpile(body)
            return `${transpile_type(ret_type)} ${name}(${args.map(([v, t]) => `${transpile_type(t)} ${v}`)})\n${indent(transpile(body))}\nend`
        },
        CompoundStmt: transpile_all_inner,
        DeclStmt: transpile_all_inner,
        VarDecl: () => {
            if (!node.inner) return ""
            if (!node.name) throw new Error("uruisdfh");
            const type = node.type?.qualType
            if (!type) throw new Error("asdjfhasdjkfhajksr");
            const name = node.name
            const value = transpile(node.inner[0])
            return `${transpile_type(type)} ${name} = ${value}`
        },
        ExprWithCleanups: transpile_first_inner,
        CXXConstructExpr: transpile_first_inner,
        MaterializeTemporaryExpr: transpile_first_inner,
        CXXOperatorCallExpr: () => {
            const op = transpile_nth_inner(0)
            const a = transpile_nth_inner(1)
            if (op == "++") return `${a} += 1`
            if (op == "--") return `${a} -= 1`
            const b = transpile_nth_inner(2)
            if (op == "[]") return `${a}[${b}]`
            return `${a} ${op} ${b}`
        },
        ImplicitCastExpr: transpile_first_inner,
        DeclRefExpr: () => {
            if (!node.referencedDecl || !node.referencedDecl.name) throw new Error("asdjkfhasrea");
            if (node.referencedDecl.name.startsWith("operator")) return node.referencedDecl.name.substr("operator".length)
            return node.referencedDecl.name
        },
        CallExpr: () => {
            if (!node.inner) throw new Error("asdfzauiezruasdfh");
            // console.log(`call to ${node.inner[0].inner && node.inner[0].inner[0].referencedDecl?.name}`);
            if ("SN".split("").includes((node.inner[0].inner && node.inner[0].inner[0].referencedDecl?.name) ?? "")) {
                return transpile_nth_inner(1)
            }
            const func = transpile_nth_inner(0)
            const args = node.inner.slice(1).map(a => transpile(a))
            return `${func}(${args.join(",")})`
        },
        CXXMemberCallExpr: () => {
            if (!node.inner) throw new Error("asdfzauiezruasdfh");
            if (node.inner[0].name?.startsWith("operator")) {
                const a = node.inner[0].inner && node.inner[0].inner[0]
                if (!a) throw new Error("zruiagshdvbnyxmc");
                return transpile(a)
            }
            const func = transpile_nth_inner(0)
            const args = node.inner.slice(1).map(a => transpile(a))
            return `${func}(${args.join(",")})`
        },
        StringLiteral: () => {
            if (!node.value) throw new Error("asurzhauisdfhkjh");
            return node.value
        },
        MemberExpr: () => {
            if (!node.name) throw new Error("asdfjahsjdkf");
            if (!node.inner) throw new Error("asdkjfhasdjuf");
            return `${transpile_first_inner()}.${node.name}`
        },
        InitListExpr: () => {
            if (!node.inner) throw new Error("asdfuezruiasfnj");
            return "[" + node.inner.map(transpile).join(", ") + "]"
        },
        IntegerLiteral: () => {
            if (!node.value) throw new Error("asdifazserzudsf");
            return node.value
        },
        CXXForRangeStmt: () => {
            if (!node.inner) throw new Error("asdjfhasdjk");
            const array = node.inner[1].inner && node.inner[1].inner[0].inner && node.inner[1].inner[0].inner[0]
            const el = node.inner[6].inner && node.inner[6].inner[0].name
            const body = node.inner[7]
            if (!array) throw new Error("asdkfgaksdhgfl");
            if (!el) throw new Error("jakdshfkjaszhre")
            if (!body) throw new Error("zsdfghscxvbnc")
            return `loop ${el} in ${transpile(array)}\n${transpile(body)}\nend`
        },
        ForStmt: () => {
            if (!node.inner) throw new Error("aiuzrioewszfhdbjn");
            const init = transpile(node.inner[0])
            const cond = transpile(node.inner[2])
            const incr = transpile(node.inner[3])
            const body = indent(transpile(node.inner[4]))
            return `${init}\nloop\nif !(${cond})\nbreak\nend\n${body}\n${incr}\nend`
        },
        CXXBoolLiteralExpr: () => {
            return node.value ? "true" : "false"
        },
        IfStmt: () => {
            if (!node.inner) throw new Error("aiuzrioewszfhdbjn");
            const cond = transpile(node.inner[0])
            const case_true = indent(transpile(node.inner[1]))
            if (node.inner.length > 2) {
                const case_false = indent(transpile(node.inner[2]))
                return `if ${cond}\n${case_true}\nelse\n${case_false}\nend`
            }
            return `if ${cond}\n${case_true}\nend`
        },
        ReturnStmt: () => `return ${transpile_first_inner()}`,
        NamespaceDecl: () => "",
        LinkageSpecDecl: () => "",
        EnumDecl: () => "",
        ClassTemplateDecl: () => "",
        FunctionTemplateDecl: () => "",

    }
    //@ts-ignore
    if (!node.kind) return ""
    const t = V[node.kind]
    if (!t) {
        console.error(node)
        throw new Error("unknown node kind: " + node.kind);
    }
    return t()
}

export function flatten<T>(a: T[][]): T[] {
    let b = [];
    for (const c of a) {
        b.push(...c)
    }
    return b
}