import { spawn } from "child_process"
import { transpile } from "./transpiler";
import { AST } from "./ast";
import { join } from "path/posix";



export async function loadAST(filename: string): Promise<AST> {
    const clang = spawn("clang", ["-Xclang", "-ast-dump=json", filename, "-fsyntax-only", "-Wno-incompatible-library-redeclaration"], {})
    let output = ""
    clang.stdout.on("data", chunk => output += chunk)
    clang.stderr.on("data", chunk => console.error(chunk.toString()))
    await new Promise<void>(r => clang.on("exit", () => r()))
    let ast: AST = JSON.parse(output)
    return ast
}

async function main() {
    const ast = await loadAST(process.argv[2])
    const code = transpile(ast)
    console.log(`string _sq = "'"`);
    console.log(`string _dq = '"'`);
    console.log(code);
    
}
main()

