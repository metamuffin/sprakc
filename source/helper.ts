
export function snake_to_camel(a: string): string {
    let b = "", u = false
    for (let c of a) {
        if (c == "_") u = true
        else {
            if (u) b += c.toUpperCase()
            else b += c.toLowerCase()
            u = false
        }
    }
    return b
}

export function snake_to_dash(a: string): string {
    return a.toLowerCase().replace("_", "-")
}

export function indent(s: string): string {
    return s.split("\n").map(l => "    " + l).join("\n")
}
