
class Any {};
class Number : public Any {
  public:
	Number(int);
	Number operator++(int);
	Number& operator++();
};
class Bool : public Any {
  public:
	Bool(bool) {
	}
	operator bool();
};
class String : public Any {
  public:
	String(const char*);
	String(Number);
	String operator[](Number);
};
class RemoteThing : public Any {
  public:
	void Info();

	Number ReadHumidity();
};

template <typename T> class Array {
  public:
	Array();
	Array(int* x);
	Array(const Array& mit);
	Array& operator++();
	Array operator++(int);
	bool operator==(const Array& rhs);
	bool operator!=(const Array& rhs);
	Array& begin();
	Array& end();
	T& operator*();
	T operator[](Number);
};

template <typename T> Number Count(Array<T>);
Number Count(String);
Array<Number> Range(Number, Number);
template <typename T> Array<Number> GetIndexes(Array<T>);
template <typename T> void Remove(Array<T>, Number);
template <typename T> void RemoveAll(Array<T>);
template <typename T> Bool HasIndex(Array<T>, Number);
template <typename T> void Append(Array<T>, T);
String Type(Number);
template <typename T> String Type(Array<T>);
String Type(String);

String S(const char*);
String S(Number);
Number N(int);
Number N(float);
Bool B(bool);
Bool B(int);
Bool B(Number);

String operator+(String, String);

Number operator+(Number, Number);
Number operator-(Number, Number);
Number operator/(Number, Number);
Number operator*(Number, Number);
Number operator%(Number, Number);
Bool operator!=(Number, Number);
Bool operator==(Number, Number);
Bool operator>=(Number, Number);
Bool operator<=(Number, Number);
Bool operator>(Number, Number);
Bool operator<(Number, Number);

Bool operator&&(Bool, Bool);
Bool operator||(Bool, Bool);

extern String _sq;
extern String _dq;

typedef int _int;
#define bool Bool
#define float Number
#define int Number
