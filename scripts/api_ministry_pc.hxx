#include "api_computer.hxx"

Array<String> GetPath(String); // ?

String GetPosition(String);
Array<String> GetAllRooms();
Array<String> GetThingsInRoom(String); //?
Array<String> GetThingsOfType(String);
String GetAction(String);
void InteractWith(String name, String target);
String GetTypeOfThing(String);
Number GetHour();
Number GetMinute();
String GetRoom();
void SetPosition(String name, String targetThing);
Array<String> GetPeople();
Bool IsKeyPressed(String key);
String Name();
Number Time();
String Type(void);

