#include "api_common.hxx"

String LoadData();
void SaveData(String);
void ClearData();
void Print(String);
void PrintS(String);
void Say(String);
String Input(String);
void Slurp();
Array<RemoteThing> GetConnections();
RemoteThing Connect(String);
Number CharToInt(String);
String IntToChar(Number);
void ClearText();
